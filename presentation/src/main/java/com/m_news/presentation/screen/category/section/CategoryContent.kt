package com.m_news.presentation.screen.category.section

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.m_news.presentation.screen.category.CategoryViewModel

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
@Composable
fun CategoryContent(
    modifier: Modifier,
    data: ArrayList<String>,
    viewModel: CategoryViewModel,
    navigateToList: (String) -> Unit,
) {
    LazyColumn(
        modifier = modifier
            .fillMaxSize(),
        content = {
            items (count = data.size){ index ->
                var title = data[index]
                CategoryItem(
                    modifier = Modifier,
                    title = title,
                    viewModel = viewModel,
                    onClick = {
                        navigateToList(title)
                    }
                )
            }
        },
        contentPadding = PaddingValues(8.dp),
    )
}