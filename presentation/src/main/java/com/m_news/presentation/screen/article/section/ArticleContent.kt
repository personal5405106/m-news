package com.m_news.presentation.screen.article.section

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.nestedscroll.NestedScrollConnection
import androidx.compose.ui.input.nestedscroll.NestedScrollSource
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.unit.dp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import com.m_news.domain.model.Articles
import com.m_news.presentation.component.EmptyData
import com.m_news.presentation.component.InputTextSearch
import com.m_news.presentation.constant.ResString
import com.m_news.presentation.constant.grey
import com.m_news.presentation.constant.grey2
import com.m_news.presentation.screen.article.ArticleViewModel
import com.m_news.presentation.screen.category.CategoryViewModel
import com.m_news.presentation.screen.source.section.SourceItem
import com.m_news.presentation.util.OnBottomReached
import com.m_news.presentation.util.noRippleClickable
import kotlinx.coroutines.launch

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
@Composable
fun ArticleContent (
    modifier: Modifier,
    data: Articles,
    viewModel: ArticleViewModel,
    navigateToDetail: (String) -> Unit,
){
    val max by viewModel.isMax.collectAsStateWithLifecycle()
    val load by viewModel.isLoading.collectAsStateWithLifecycle()
    val query by viewModel.query.collectAsStateWithLifecycle()

    val isRefreshing =  remember { mutableStateOf(false) }
    val isShowButtonUp =  remember { mutableStateOf(false) }
    val listState = rememberLazyListState()
    val coroutineScope = rememberCoroutineScope()
    val nestedScrollConnection = remember {
        object : NestedScrollConnection {
            override fun onPostScroll(
                consumed: Offset,
                available: Offset,
                source: NestedScrollSource
            ): Offset {
                if(listState.firstVisibleItemIndex > 0){
                    isShowButtonUp.value = true
                }else{
                    isShowButtonUp.value = false
                }
                return super.onPostScroll(consumed, available, source)
            }
        }
    }
    Box{
        SwipeRefresh(
            modifier = Modifier
                .fillMaxWidth()
                .nestedScroll(nestedScrollConnection),
            state = rememberSwipeRefreshState(isRefreshing = isRefreshing.value),
            onRefresh = {
                isRefreshing.value = true
                viewModel.refresh()
                isRefreshing.value = false
            }
        ) {
            LazyColumn(
                modifier = modifier
                    .fillMaxSize(),
                state = listState,
                content = {
                    item{
                        InputTextSearch(
                            search = query,
                            onClear = {
                                viewModel.searching("")
                            },
                            onSearch = { search ->
                                viewModel.searching(search)
                            }
                        )
                    }
                    if(data.articles.size == 0){
                        item{ EmptyData(message = ResString.NO_ARTICLE) }
                    }
                    items (count = data.articles.size){ index ->
                        var article = data.articles[index]
                        ArticleItem(
                            modifier = Modifier,
                            data = article,
                            viewModel = viewModel,
                            onClick = {
                                navigateToDetail(article.url!! + "&" + article.title!!)
                            }
                        )
                    }
                },
                contentPadding = PaddingValues(8.dp),
            )
        }

        Column(
            modifier = modifier
                .fillMaxSize(),
            verticalArrangement = Arrangement.Bottom
        ) {
            if(isShowButtonUp.value){
                Row(
                    modifier = Modifier
                        .padding(horizontal = 10.dp, vertical = 30.dp)
                        .fillMaxWidth(),
                    horizontalArrangement = Arrangement.End
                ) {
                    Box(
                        modifier = Modifier
                            .width(50.dp)
                            .height(50.dp)
                            .background(
                                color = Color.LightGray,
                                shape = RoundedCornerShape(100),
                            )
                            .noRippleClickable {
                                coroutineScope.launch {
                                    listState.scrollToItem(index = 0)
                                }
                            },
                        contentAlignment = Alignment.Center
                    ) {
                        Icon(
                            imageVector = Icons.Default.KeyboardArrowUp,
                            contentDescription = "scrollUp",
                            tint = Color.Black,
                            modifier = Modifier
                                .width(40.dp)
                                .height(40.dp)
                                .padding(5.dp),
                        )
                    }
                }
            }
        }
    }
    listState.OnBottomReached (buffer = 2){
        if(!max && !load){
            viewModel.addCounterPage()
        }
    }
}