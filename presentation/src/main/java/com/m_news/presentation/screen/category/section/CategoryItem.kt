package com.m_news.presentation.screen.category.section

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.m_news.presentation.screen.category.CategoryViewModel

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
@Composable
fun CategoryItem(
    modifier: Modifier,
    title:String,
    viewModel: CategoryViewModel,
    onClick : () -> Unit
) {
    OutlinedButton(
        modifier = Modifier.fillMaxWidth().padding(all = 5.dp),
        border = BorderStroke(0.5.dp, Color.Gray),
        shape = RoundedCornerShape(4.dp),
        onClick = onClick
    ){
        Text(
            text = title,
            fontSize = 16.sp,
            color = Color.Black,
            textAlign = TextAlign.Center,
        )
    }
}