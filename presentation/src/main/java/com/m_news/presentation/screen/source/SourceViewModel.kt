package com.m_news.presentation.screen.source

import androidx.compose.runtime.MutableState
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.m_news.domain.model.Source
import com.m_news.domain.model.Sources
import com.m_news.domain.usecase.GetSourcesUseCase
import com.m_news.presentation.util.UiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
@HiltViewModel
class SourceViewModel @Inject constructor(
    private val getSourcesUseCase: GetSourcesUseCase
):ViewModel() {
    private val _uiState : MutableStateFlow<UiState<Sources>> = MutableStateFlow(UiState.Loading())
    val uiState : StateFlow<UiState<Sources>> = _uiState

    private val _isLoading = MutableStateFlow(false)
    val isLoading = _isLoading.asStateFlow()

    private val _query = MutableStateFlow("")
    val query = _query.asStateFlow()
    var sources = ArrayList<Source>()

    fun searching(search : String){
        _query.value = search
        viewModelScope.launch {
            if(_uiState.value is UiState.Success){
                var list = ArrayList<Source>()
                sources.forEach { source ->
                    var isAdd = true
                    if(!search.equals("")){
                        isAdd = false
                        if(source.name!!.lowercase().contains(search.lowercase())){
                            isAdd = true
                        }
                    }
                    if(isAdd){
                        list.add(source)
                    }
                }

                var response = _uiState.value
                response.data?.sources = list
                _uiState.value = UiState.Success(response.data!!)
            }
        }
    }

    fun refresh(){
        _uiState.value = UiState.Loading()
    }

    fun getSources(category:String){
        _isLoading.value = true
        CoroutineScope(Dispatchers.IO).launch {
            try {
                getSourcesUseCase.execute(category)
                    .catch {
                        _isLoading.value = false
                        _uiState.value = UiState.Error(it.message.toString())
                    }.collect{response ->
                        _isLoading.value = false
                        sources = response.sources
                        _uiState.value = UiState.Success(response)
                    }
            }catch (e:Exception){
                _isLoading.value = false
                _uiState.value = UiState.Error(e.message.toString())
            }
        }
    }
}