package com.m_news.presentation.screen.category

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.runtime.*
import androidx.compose.ui.text.style.TextOverflow
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.m_news.presentation.component.EmptyData
import com.m_news.presentation.component.ProgressLoading
import com.m_news.presentation.constant.ResString
import com.m_news.presentation.constant.primary
import com.m_news.presentation.screen.category.section.CategoryContent
import com.m_news.presentation.util.UiState

/***
 * Created By Mohammad Toriq on 05/02/2024
 */

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CategoryScreen (
    viewModel: CategoryViewModel = hiltViewModel(),
    navigateToList: (String) -> Unit,
){
    val load by viewModel.isLoading.collectAsStateWithLifecycle()

    Scaffold(
        topBar ={
            Surface (shadowElevation = 1.dp){
                CenterAlignedTopAppBar(
                    title = {
                        Text(
                            text = "Categories Source",
                            fontSize = 16.sp,
                            fontWeight = FontWeight.Bold,
                            maxLines = 1,
                            overflow = TextOverflow.Ellipsis,
                            modifier = Modifier.padding(horizontal = 10.dp)
                        )
                    },
                    colors = TopAppBarDefaults.mediumTopAppBarColors(
                        containerColor = Color.White,
                        titleContentColor = Color.Black,
                    ),
                )
            }
        },
        content = {
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color.White)
                    .padding(it)
                    .padding(5.dp)
            ) {

                viewModel.uiState.collectAsState(initial = UiState.Loading()).value.let { uiState ->
                    when (uiState) {
                        is UiState.Loading -> {
                            ProgressLoading()
                            if(!load){
                                viewModel.loading()
                            }
                        }

                        is UiState.Success -> {
                            if(uiState.data == null){
                                EmptyData(message = ResString.DATA_NOT_FOUND)
                            }else{
                                if(uiState.data.isNullOrEmpty()){
                                    EmptyData(message = ResString.NO_CATEGORY)
                                }else{
                                    CategoryContent(
                                        modifier = Modifier,
                                        data = uiState.data,
                                        viewModel = viewModel,
                                        navigateToList = navigateToList
                                    )
                                }
                            }
                        }

                        is UiState.Error -> {
                            EmptyData(message = ResString.DATA_NOT_FOUND)
                        }
                    }
                }
            }
        }
    )
}