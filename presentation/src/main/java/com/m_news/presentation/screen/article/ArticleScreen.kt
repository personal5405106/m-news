package com.m_news.presentation.screen.article

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.m_news.presentation.component.EmptyData
import com.m_news.presentation.component.ProgressLoading
import com.m_news.presentation.constant.ResString
import com.m_news.presentation.screen.article.section.ArticleContent
import com.m_news.presentation.screen.source.section.SourceContent
import com.m_news.presentation.util.UiState

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ArticleScreen(
    source:String,
    title:String,
    viewModel: ArticleViewModel = hiltViewModel(),
    navigateToDetail : (String) -> Unit,
    navigateBack : () -> Unit
) {
    val load by viewModel.isLoading.collectAsStateWithLifecycle()

    BackHandler {
        navigateBack()
    }
    Scaffold(
        topBar ={
            Surface (shadowElevation = 1.dp){
                CenterAlignedTopAppBar(
                    title = {
                        Text(
                            text = title,
                            fontSize = 16.sp,
                            fontWeight = FontWeight.Bold,
                            maxLines = 1,
                            overflow = TextOverflow.Ellipsis,
                            modifier = Modifier.padding(horizontal = 10.dp)
                        )
                    },
                    colors = TopAppBarDefaults.mediumTopAppBarColors(
                        containerColor = Color.White,
                        titleContentColor = Color.Black,
                    ),
                    navigationIcon = {
                        IconButton(
                            onClick = navigateBack
                        ) {
                            Image(
                                imageVector = Icons.Filled.ArrowBack,
                                colorFilter = ColorFilter.tint(Color.Black),
                                contentDescription = "Back"
                            )
                        }
                    },
                )
            }
        },
        content = {
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color.White)
                    .padding(it)
                    .padding(5.dp)
            ) {

                viewModel.uiState.collectAsState(initial = UiState.Loading()).value.let { uiState ->
                    when (uiState) {
                        is UiState.Loading -> {
                            ProgressLoading()
                            if(!load){
                                viewModel.getArticles(source)
                            }
                        }

                        is UiState.Success -> {
                            if(uiState.data == null){
                                EmptyData(message = ResString.DATA_NOT_FOUND)
                            }else{
                                ArticleContent(
                                    modifier = Modifier,
                                    data = uiState.data,
                                    viewModel = viewModel,
                                    navigateToDetail = navigateToDetail
                                )
                            }
                        }

                        is UiState.Error -> {
                            EmptyData(message = ResString.DATA_NOT_FOUND)
                        }
                    }
                }
            }
        }
    )
}