package com.m_news.presentation.screen.category

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.m_news.presentation.constant.Categories
import com.m_news.presentation.util.UiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
@HiltViewModel
class CategoryViewModel  @Inject constructor(
): ViewModel() {
    private val _uiState: MutableStateFlow<UiState<ArrayList<String>>> = MutableStateFlow(UiState.Loading())
    val uiState: StateFlow<UiState<ArrayList<String>>> = _uiState

    private val _isLoading = MutableStateFlow(false)
    val isLoading = _isLoading.asStateFlow()

    fun getCategories() {
        _uiState.value = UiState.Success(Categories.getCategories())
        _isLoading.value = false
    }

    fun loading() {
        viewModelScope.launch {
            _isLoading.value = true
            delay(500)
            getCategories()

        }
    }

}