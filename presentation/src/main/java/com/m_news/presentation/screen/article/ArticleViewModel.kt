package com.m_news.presentation.screen.article

import androidx.lifecycle.ViewModel
import com.m_news.domain.model.Articles
import com.m_news.domain.usecase.GetArticlesUseCase
import com.m_news.presentation.util.UiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
@HiltViewModel
class ArticleViewModel @Inject constructor(
    private val getArticlesUseCase: GetArticlesUseCase
):ViewModel() {
    private val _uiState : MutableStateFlow<UiState<Articles>> = MutableStateFlow(UiState.Loading())
    val uiState : StateFlow<UiState<Articles>> = _uiState

    private val _isLoading  = MutableStateFlow(false)
    val isLoading = _isLoading.asStateFlow()

    private val _isMax  = MutableStateFlow(false)
    val isMax = _isMax.asStateFlow()

    private val _page  = MutableStateFlow(1)
    val page = _page.asStateFlow()

    private val _query  = MutableStateFlow("")
    val query = _query.asStateFlow()

    var source = ""
    var remainingArticle = 0

    fun refresh(){
        _page.value = 1
        _uiState.value = UiState.Loading()
    }

    fun searching(search:String){
        _query.value = search
        refresh()
    }

    fun addCounterPage(){
        _page.value ++
        getArticles(source)
    }

    fun getArticles(source:String){
        this.source = source
        _isLoading.value = true
        CoroutineScope(Dispatchers.IO).launch {
            try {
                var q :String? = query.value
                if(query.value.equals("")){
                    q = null
                }
                var params = listOf<String?>(q,source,page.value.toString(),"20")
                getArticlesUseCase.execute(params)
                    .catch {
                        _isLoading.value = false
                        _isMax.value = true
                        _uiState.value = UiState.Error(it.message.toString())
                    }.collect{response->
                        var list = _uiState.value.data?.articles
                        if(list == null){
                            list = arrayListOf()
                        }
                        if(page.value == 1){
                            list = arrayListOf()
                            remainingArticle = response.totalResults
                        }
                        response.articles.forEach {
                            list.add(it)
                        }
                        remainingArticle = remainingArticle - response.articles.size

                        response.articles = list

                        _isLoading.value = false
                        _isMax.value = false
                        if(remainingArticle == 0){
                            _isMax.value = true
                        }
                        _uiState.value = UiState.Success(response)

                    }
            }catch (e:Exception){
                _isLoading.value = false
                _isMax.value = true
                _uiState.value = UiState.Error(e.message.toString())
            }
        }
    }
}