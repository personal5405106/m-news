package com.m_news.presentation.screen.source.section

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.m_news.domain.model.Source
import com.m_news.presentation.screen.category.CategoryViewModel
import com.m_news.presentation.screen.source.SourceViewModel

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
@Composable
fun SourceItem(
    modifier: Modifier,
    data:Source,
    viewModel: SourceViewModel,
    onClick : () -> Unit
) {
    OutlinedButton(
        modifier = Modifier.padding(all = 5.dp),
        border = BorderStroke(0.5.dp, Color.Gray),
        shape = RoundedCornerShape(4),
        onClick = onClick
    ){
        Row (
            modifier = Modifier
                .fillMaxWidth()
                .padding(5.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween
        ){
            Text(
                text = data.name!!,
                fontSize = 14.sp,
                color = Color.Black,
                textAlign = TextAlign.Center,
            )
            Icon(
                Icons.Filled.ArrowForward,
                modifier = Modifier.size(20.dp),
                tint = Color.Black,
                contentDescription = "",
            )
        }
    }
}