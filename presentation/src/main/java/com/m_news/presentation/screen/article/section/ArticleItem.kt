package com.m_news.presentation.screen.article.section

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.m_news.domain.model.Article
import com.m_news.domain.model.Source
import com.m_news.presentation.constant.FormatTime
import com.m_news.presentation.constant.grey
import com.m_news.presentation.screen.article.ArticleViewModel
import com.m_news.presentation.screen.category.CategoryViewModel
import com.m_news.presentation.screen.source.SourceViewModel
import com.m_news.presentation.util.Util
import com.m_news.presentation.util.noRippleClickable

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
@Composable
fun ArticleItem(
    modifier: Modifier,
    data:Article,
    viewModel: ArticleViewModel,
    onClick : () -> Unit
) {
    Column(
        modifier = Modifier
            .padding(all = 5.dp)
            .clip(
                shape = RoundedCornerShape(4.dp),
            )
            .fillMaxWidth()
            .noRippleClickable {
                onClick()
            }
    ){
        if(data.urlToImage!=null){
            AsyncImage(
                model = data.urlToImage,
                contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .fillMaxWidth()
                    .height(200.dp)
            )
        }
        if(data.publishedAt!=null){
            Spacer(modifier = Modifier.height(10.dp))
            Text(
                text = "Publish : "+changeFormatDate(data.publishedAt!!),
                fontSize = 11.sp,
                color = grey,
                modifier = Modifier
                    .padding(horizontal = 10.dp)
                    .align(Alignment.End),
            )
        }
        Spacer(modifier = Modifier.height(10.dp))
        Text(
            text = data.title!!,
            fontSize = 16.sp,
            fontWeight = FontWeight.Bold,
            color = Color.Black,
            modifier = Modifier
                .padding(horizontal = 5.dp),
        )
        Spacer(modifier = Modifier.height(10.dp))
        Text(
            text = data.description?: data.content?: "",
            fontSize = 12.sp,
            maxLines = 3,
            overflow = TextOverflow.Ellipsis,
            color = Color.Black,
            modifier = Modifier
                .padding(horizontal = 10.dp)
                .align(Alignment.End),
        )
        if(data.author!=null){
            Spacer(modifier = Modifier.height(15.dp))
            Text(
                text = "Author : "+data.author,
                fontSize = 12.sp,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                color = grey,
                fontWeight = FontWeight.SemiBold,
                modifier = Modifier
                    .padding(horizontal = 10.dp),
            )
        }
        Spacer(modifier = Modifier.height(10.dp))
        Divider(thickness = 0.5.dp, color = Color.Gray)
    }
}
fun changeFormatDate(publishedAt: String) : String{
    var inFormat = FormatTime.DATE_OUT_FORMAT_DEF0
    if(publishedAt.contains(".")){
        inFormat = FormatTime.DATE_OUT_FORMAT_DEF1
    }
    return Util.convertDate(publishedAt, inFormat, FormatTime.DATE_OUT_FORMAT_DEF3)
}