package com.m_news.presentation.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.m_news.presentation.screen.article.ArticleScreen
import com.m_news.presentation.screen.category.CategoryScreen
import com.m_news.presentation.screen.detailarticle.DetailArticleScreen
import com.m_news.presentation.screen.source.SourceScreen
import com.m_news.presentation.screen.splash.SplashScreen

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
@Composable
fun MainNavHost(
    navController: NavHostController
) {
    NavHost(
        navController = navController,
        startDestination = Screen.CategoriesScreen.route
    ) {
        composable(route = Screen.SplashScreen.route) {
            SplashScreen(
                navigateToMain = {
                    navController.navigate(Screen.CategoriesScreen.route){
                        popUpTo(Screen.SplashScreen.route){
                            inclusive = true
                        }
                    }
                }
            )
        }
        composable(route = Screen.CategoriesScreen.route) {
            CategoryScreen(
                navigateToList = { title ->
                    navController.navigate(
                        Screen.SourcesScreen
                            .sendData(title)
                    )
                }
            )
        }
        composable(route = Screen.SourcesScreen.route){
            var category = it.arguments?.getString("category") ?: ""
            SourceScreen(
                category,
                navigateToList = { source, title ->
                    navController.navigate(
                        Screen.ArticlesScreen
                            .sendData(source,title)
                    )
                },
                navigateBack={
                    navController.navigateUp()
                }
            )
        }
        composable(route = Screen.ArticlesScreen.route){
            var source = it.arguments?.getString("source") ?: ""
            var title = it.arguments?.getString("title") ?: ""
            ArticleScreen(
                title = title,
                source = source,
                navigateToDetail = { url_title ->
                    navController.navigate(
                        Screen.DetailArticleScreen
                            .sendData(url_title)
                    )
                },
                navigateBack={
                    navController.navigateUp()
                }
            )
        }
        composable(route = Screen.DetailArticleScreen.route){
            var url_title = it.arguments?.getString("url_title") ?: ""
            DetailArticleScreen(
                url_title,
                navigateBack={
                    navController.navigateUp()
                })
        }
    }
}