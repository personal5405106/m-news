package com.m_news.presentation.constant

import androidx.compose.ui.graphics.Color

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
val primary = Color(0xFFFFFFFF)
val secondary = Color(0xFFFFFFFF)
val tertiary = Color(0xFFFFFFFF)

val grey = Color(0xFF888888)
val grey2 = Color(0xA0888888)
val blue = Color(0xFF1A73E8)
val blue2 = Color(0xFF1AB8E8)