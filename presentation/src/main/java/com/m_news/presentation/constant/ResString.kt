package com.m_news.presentation.constant


/***
 * Created By Mohammad Toriq on 05/02/2024
 */
class ResString {
    companion object {
        const val APP_NAME = "M-News"
        const val LOADING = "Loading …"
        const val DATA_NOT_FOUND = "Data not Found"
        const val NO_CATEGORY = "Category not Found"
        const val NO_SOURCE = "Source not Found"
        const val NO_ARTICLE = "Article not Found"
    }
}

class FormatTime{
    companion object{
        val DATE_OUT_FORMAT_DEF0 = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        val DATE_OUT_FORMAT_DEF1 = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        val DATE_OUT_FORMAT_DEF2 = "yyyy-MM-dd"
        val DATE_OUT_FORMAT_DEF3 = "MMM dd, yyyy"
    }
}

class Categories{
    companion object{
        fun getCategories() : ArrayList<String>{
            return arrayListOf(
                "Business",
                "Entertainment",
                "General",
                "Health",
                "Science",
                "Sports",
                "Technology")
        }
    }
}