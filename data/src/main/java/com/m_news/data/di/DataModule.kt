package com.m_news.data.di

import com.m_news.data.repository.RepositoryImpl
import com.m_news.data.source.remote.ApiService
import com.m_news.domain.repository.Repository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
@Module
@InstallIn(SingletonComponent::class)
object DataModule {
    @Provides
    @Singleton
    fun provideRepositoryImpl(
        apiService: ApiService,
    ): Repository = RepositoryImpl(apiService)
}