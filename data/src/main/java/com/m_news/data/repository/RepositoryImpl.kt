package com.m_news.data.repository

import com.m_news.data.source.remote.ApiService
import com.m_news.data.source.remote.dto.toArticles
import com.m_news.data.source.remote.dto.toSources
import com.m_news.domain.model.Articles
import com.m_news.domain.model.Sources
import com.m_news.domain.repository.Repository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
class RepositoryImpl constructor(
    private val apiService: ApiService
) : Repository
{
    override suspend fun getSources(category: String?): Flow<Sources> {
        return flow {
            var data = apiService.getSources(category)
            emit(data.toSources())
        }
    }

    override suspend fun getSourceArticles(
        q: String?,
        sources: String,
        page: Int,
        pageSize: Int
    ): Flow<Articles> {
        return flow {
            var data = apiService.getSourceArticles(q,sources,page,pageSize)
            emit(data.toArticles())
        }
    }

}