package com.m_news.data.source.remote.dto

import com.google.gson.annotations.SerializedName
import com.m_news.domain.model.Article

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
data class ArticleDto (
    @SerializedName("source")
    val source :SourceDto,
    @SerializedName("author")
    val author :String?,
    @SerializedName("title")
    val title :String?,
    @SerializedName("description")
    val description :String?,
    @SerializedName("url")
    val url :String?,
    @SerializedName("urlToImage")
    val urlToImage :String?,
    @SerializedName("publishedAt")
    val publishedAt :String?,
    @SerializedName("content")
    val content :String?,
)
fun ArticleDto.toArticle(): Article {
    return Article(
        source = source.toSource(),
        author = author,
        title = title,
        description = description,
        url = url,
        urlToImage = urlToImage,
        publishedAt = publishedAt,
        content = content,
    )
}