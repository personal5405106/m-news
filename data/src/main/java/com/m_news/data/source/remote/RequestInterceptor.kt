package com.m_news.data.source.remote

import okhttp3.Interceptor
import okhttp3.Response

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
class RequestInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val newUrl = originalRequest.url
            .newBuilder()
            .addQueryParameter("apiKey", ApiService.API_KEY)
            .build()
        val request = originalRequest.newBuilder()
            .url(newUrl)
            .build()
        return chain.proceed(request)
    }
}