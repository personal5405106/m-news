package com.m_news.data.source.remote

import com.m_news.data.source.remote.dto.ArticlesDto
import com.m_news.data.source.remote.dto.SourcesDto
import retrofit2.http.GET
import retrofit2.http.Query

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
interface ApiService {
    companion object {
        const val BASE_URL: String = "https://newsapi.org/v2/"
        const val API_KEY: String = "270ab5ea3209412d8761e1d83c8ad563"
    }

    @GET("top-headlines/sources")
    suspend fun getSources(
        @Query(value = "category", encoded = true) category: String?,
    ): SourcesDto

    @GET("everything")
    suspend fun getSourceArticles(
        @Query(value = "q", encoded = true) q: String? = null,
        @Query(value = "sources", encoded = true) sources: String? = null,
        @Query(value = "page", encoded = true) page: Int,
        @Query(value = "pageSize", encoded = true) pageSize: Int,
    ): ArticlesDto
}