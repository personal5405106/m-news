package com.m_news.data.source.remote.dto

import com.google.gson.annotations.SerializedName
import com.m_news.domain.model.Source

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
data class SourceDto (
    @SerializedName("id")
    val id :String?,
    @SerializedName("name")
    val name :String?,
    @SerializedName("description")
    val description :String?,
    @SerializedName("url")
    val url :String?,
    @SerializedName("category")
    val category :String?,
    @SerializedName("language")
    val language :String?,
    @SerializedName("country")
    val country :String?,
)

fun SourceDto.toSource(): Source{
    return Source(
        id = id,
        name = name,
        description = description,
        url = url,
        category = category,
        language = language,
        country = country,
    )
}