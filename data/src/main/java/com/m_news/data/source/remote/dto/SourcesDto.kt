package com.m_news.data.source.remote.dto

import com.google.gson.annotations.SerializedName
import com.m_news.domain.model.Source
import com.m_news.domain.model.Sources

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
data class SourcesDto (
    @SerializedName("sources")
    val sources :List<SourceDto>,
    @SerializedName("status")
    val status :String?,
)
fun SourcesDto.toSources(): Sources {
    var arrayList = ArrayList<Source>()
    var list = sources.map { it.toSource() }
    list.forEach {
        arrayList.add(it)
    }
    return Sources(
        sources = arrayList,
        status = status,
    )
}