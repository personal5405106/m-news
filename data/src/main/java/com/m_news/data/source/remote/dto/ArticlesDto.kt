package com.m_news.data.source.remote.dto

import com.google.gson.annotations.SerializedName
import com.m_news.domain.model.Article
import com.m_news.domain.model.Articles

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
data class ArticlesDto (
    @SerializedName("articles")
    val articles :List<ArticleDto>,
    @SerializedName("status")
    val status :String?,
    @SerializedName("totalResults")
    val totalResults :Int = 0,
)
fun ArticlesDto.toArticles(): Articles {
    var arrayList = ArrayList<Article>()
    var list = articles.map { it.toArticle() }
    list.forEach {
        arrayList.add(it)
    }
    return Articles(
        articles = arrayList,
        status = status,
        totalResults = totalResults,
    )
}