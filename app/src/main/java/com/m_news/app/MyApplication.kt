package com.m_news.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
@HiltAndroidApp
class MyApplication: Application()