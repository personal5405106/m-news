package com.m_news.domain.usecase

import com.m_news.domain.model.Articles
import com.m_news.domain.repository.Repository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
class GetArticlesUseCase @Inject constructor(
    private val repository: Repository
):BaseUseCase<List<String?> , Flow<Articles>>(){
    override suspend fun execute(params: List<String?>): Flow<Articles> {
        return repository.getSourceArticles(params[0],params[1]!!,params[2]!!.toInt(),params[3]!!.toInt())
    }
}