package com.m_news.domain.usecase

import com.m_news.domain.model.Sources
import com.m_news.domain.repository.Repository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
class GetSourcesUseCase  @Inject constructor(
    private val repository: Repository
):BaseUseCase<String? , Flow<Sources>>(){
    override suspend fun execute(params: String?): Flow<Sources> {
        return repository.getSources(params)
    }
}