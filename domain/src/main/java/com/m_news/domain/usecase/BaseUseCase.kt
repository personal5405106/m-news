package com.m_news.domain.usecase

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
abstract class BaseUseCase<in Params, out T> {
    abstract suspend fun execute(params: Params): T
}