package com.m_news.domain.model

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
data class Articles (
    var articles :ArrayList<Article> = arrayListOf(),
    val status :String?,
    val totalResults :Int = 0,
)