package com.m_news.domain.model

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
data class Source (
    val id :String?,
    val name :String?,
    val description :String?,
    val url :String?,
    val category :String?,
    val language :String?,
    val country :String?,
)