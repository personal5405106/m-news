package com.m_news.domain.model

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
data class Sources (
    var sources :ArrayList<Source> = arrayListOf(),
    val status :String?,
)