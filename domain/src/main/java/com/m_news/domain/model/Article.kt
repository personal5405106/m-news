package com.m_news.domain.model

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
data class Article (
    val source :Source,
    val author :String?,
    val title :String?,
    val description :String?,
    val url :String?,
    val urlToImage :String?,
    val publishedAt :String?,
    val content :String?,
)