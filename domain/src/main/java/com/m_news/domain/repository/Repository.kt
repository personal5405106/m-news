package com.m_news.domain.repository

import com.m_news.domain.model.Articles
import com.m_news.domain.model.Sources
import kotlinx.coroutines.flow.Flow

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
interface Repository {
    suspend fun getSources(category: String?): Flow<Sources>

    suspend fun getSourceArticles(
        q :String?,
        sources:String,
        page: Int = 1,
        pageSize: Int = 20): Flow<Articles>
}